# Run the following commands to setup and start the server
Extract the zip file and cd into `blog-lite` folder to set up and run the application.

## Creating a database
Run the following commands to create a database with some data (if not already created) `sqlite3 database.sqlite3`. Then run the following commands.
```
BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "comment" (
	"comment_id"	INTEGER NOT NULL UNIQUE,
	"username"	TEXT,
	"post_id"	INTEGER,
	"text"	TEXT,
	"timestamp"	TEXT,
	FOREIGN KEY("username") REFERENCES "users"("username"),
	PRIMARY KEY("comment_id"),
	FOREIGN KEY("post_id") REFERENCES "post"("post_id")
);
CREATE TABLE IF NOT EXISTS "follow" (
	"f_id"	INTEGER NOT NULL UNIQUE,
	"follower"	TEXT,
	"following"	TEXT,
	FOREIGN KEY("following") REFERENCES "users"("username"),
	PRIMARY KEY("f_id" AUTOINCREMENT),
	FOREIGN KEY("follower") REFERENCES "users"("username")
);
CREATE TABLE IF NOT EXISTS "like" (
	"l_id"	INTEGER NOT NULL UNIQUE,
	"post_id"	INTEGER,
	"username"	TEXT,
	FOREIGN KEY("post_id") REFERENCES "post"("post_id"),
	PRIMARY KEY("l_id" AUTOINCREMENT),
	FOREIGN KEY("username") REFERENCES "users"("username")
);
CREATE TABLE IF NOT EXISTS "post" (
	"post_id"	INTEGER NOT NULL UNIQUE,
	"username"	TEXT,
	"title"	Text,
	"caption"	TEXT,
	"image"	TEXT,
	"timestamp"	TEXT,
	"likes"	INTEGER,
	"comments"	INTEGER,
	FOREIGN KEY("username") REFERENCES "users"("username"),
	PRIMARY KEY("post_id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "users" (
	"username"	TEXT NOT NULL UNIQUE,
	"name"	TEXT,
	"password"	TEXT,
	"followers"	INTEGER,
	"following"	INTEGER,
	"posts"	INTEGER,
	PRIMARY KEY("username")
);
```
Certain data can be added (not neccessary exactly the same, but the images match the images in the static folder).
```
INSERT INTO "comment" ("comment_id","username","post_id","text","timestamp") VALUES (1,'sanjay',1,'Very interesting','2022-11-27 15:58:08.735210');
INSERT INTO "follow" ("f_id","follower","following") VALUES (8,'sunder','sanjay');
INSERT INTO "follow" ("f_id","follower","following") VALUES (10,'varun','sanjay');
INSERT INTO "like" ("l_id","post_id","username") VALUES (3,1,'sanjay');
INSERT INTO "post" ("post_id","username","title","caption","image","timestamp","likes","comments") VALUES (1,'sunder','Panda','Is it eating?','1_panda.jpg','2022-11-26 15:11:26.146966',1,1);
INSERT INTO "post" ("post_id","username","title","caption","image","timestamp","likes","comments") VALUES (2,'sanjay','Deer','Observing the surroundings','2_deer.jpg','2022-11-27 15:17:44.759738',0,0);
INSERT INTO "post" ("post_id","username","title","caption","image","timestamp","likes","comments") VALUES (8,'varun','Cat','staring at you isn''t it?','8_cat.jpg','2022-11-27 16:05:35.760586',0,0);
INSERT INTO "users" ("username","name","password","followers","following","posts") VALUES ('sunder','Sunderarajan Venkatavaradan','1234',0,1,1);
INSERT INTO "users" ("username","name","password","followers","following","posts") VALUES ('sanjay','M. Sanjay','1234',2,0,1);
INSERT INTO "users" ("username","name","password","followers","following","posts") VALUES ('varun','Varun Sunderarajan','1234',0,1,1);
COMMIT;
```

## Creating the virtual environment (if not already created)
The current .env file is the virtual environment for my ubuntu 20.04 system. Please delete and re-create it if it does not work well.

```bash
apt install python3.8-venv
python3.8 -m venv .env

```

## Launching the virtual environment
```bash
source .env/bin/activate

```
## Installing the required packeges
```bash
pip install --upgrade pip
pip install -r requirements.txt
```

## Starting the server
```bash
python main.py
```
