from flask import current_app as app
from datetime import datetime
from flask import Flask, redirect, render_template, request, make_response

from application.models import *

def getUser():
    '''
    Check if the user is logged in with a valid username and password.
    If username and password is correct: return user
    If username and password don't match, return False
    If username not found: return None
    '''
    username=request.cookies.get('username')
    password=request.cookies.get('password')
    u=Users.query.filter_by(username=username).first()
    if bool(Users.query.filter_by(username=username).first()):
        if password==u.password:
            return u
        else:
            return False
    else:
        return None


def like_post(user, post):
    liking=Likes.query.filter_by(post_id=post.post_id, username=user.username).first()
    if not bool(liking):
        liking = Likes(post_id=post.post_id, username=user.username)
        db.session.add(liking)
        post.likes+=1
        db.session.commit()

def unlike_post(user, post):
    liking=Likes.query.filter_by(post_id=post.post_id, username=user.username).first()
    if bool(liking):
        db.session.delete(liking)
        post.likes-=1
        db.session.commit()

def add_comment(user, post, text):
    c=Comments(
        username=user.username,
        post_id=post.post_id,
        text=text,
        timestamp=str(datetime.now())
        )
    db.session.add(c)
    post.comments+=1
    db.session.commit()

def delete_comment(comment_id):
    comment=Comments.query.filter_by(comment_id=comment_id).first()
    if comment:
        post=Posts.query.get(comment.post_id)
        db.session.delete(comment)
        post.comments-=1
        db.session.commit()

def follow_user(follower, following):
    f=Follow.query.filter_by(follower=follower.username, following=following.username).first()
    if not f:
       f = Follow(follower=follower.username, following=following.username)
       db.session.add(f)
       follower.following +=1
       following.followers +=1
       db.session.commit()

def unfollow_user(follower, following):
    f=Follow.query.filter_by(follower=follower.username, following=following.username).first()
    if f:
       db.session.delete(f)
       follower.following -=1
       following.followers -=1
       db.session.commit()

def delete_post(post):
    # delete all the likes associated with the post
    l = Likes.query.filter_by(post_id=post.post_id).all()
    for i in l:
        db.session.delete(i)
    db.session.commit()
    # delete all the comments associated with the post
    c = Comments.query.filter_by(post_id=post.post_id).all()
    for i in c:
        db.session.delete(i)
    db.session.commit()
    # decrement the user's number of posts
    u=Users.query.get(post.username)
    u.posts-=1
    db.session.commit()
    # now delete the post itself
    os.remove("static/images/"+post.image)
    db.session.delete(post)
    
    db.session.commit()

def delete_user(user):
    # delete all posts created by the user
    p = Posts.query.filter_by(username=user.username).all()
    for i in p:
        delete_post(i)
        
    # delete all the comments associated with the user
    c = Comments.query.filter_by(username=user.username).all()
    for i in c:
        delete_comment(i.comment_id)
    
    # unlike posts
    l = Likes.query.filter_by(username=user.username).all()
    for i in l:
        post=Posts.query.get(i.post_id)
        post.likes-=1
        db.session.delete(i)
    
           
    # unfollow people  
    flng = Follow.query.filter_by(follower=user.username).all()
    for i in flng:
        user2 = Users.query.get(i.following)
        user2.followers-=1
        db.session.delete(i)
    
    # stop people following you
    flrs = Follow.query.filter_by(following=user.username).all()
    for i in flrs:
        user2 = Users.query.get(i.follower)
        user2.following-=1
        db.session.delete(i)
    
    # Now, delete the user
    db.session.delete(user)
    db.session.commit()

def following_list(user):
    '''
    return a list usernames of users 'user' is following
    '''
    result=[]
    if user:
        f=Follow.query.filter_by(follower=user.username).all()
        for i in f:
            result.append(i.following)
    return result
        
