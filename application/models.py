import os
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.ext.declarative import declarative_base
db=SQLAlchemy()

class Users(db.Model):
    __tablename__='users'
    username=db.Column(db.String, unique=True, primary_key=True, nullable=False)
    name=db.Column(db.String)
    password=db.Column(db.String)
    followers=db.Column(db.Integer)
    following=db.Column(db.Integer)
    posts=db.Column(db.Integer)

class Posts(db.Model):
    __tablename__='post'
    post_id = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False)
    username=db.Column(db.String, db.ForeignKey("users.username") )
    title=db.Column(db.String)
    caption=db.Column(db.String)
    image=db.Column(db.String)
    timestamp=db.Column(db.String)
    likes=db.Column(db.Integer)
    comments=db.Column(db.Integer)

class Comments(db.Model):
    __tablename__='comment'
    comment_id = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False)
    username=db.Column(db.Text, db.ForeignKey("users.username") )
    post_id=db.Column(db.Integer, db.ForeignKey("post.post_id") )
    text=db.Column(db.Text)
    timestamp=db.Column(db.Text)

class Follow(db.Model):
    __tablename__='follow'
    f_id = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False)
    follower=db.Column(db.String, db.ForeignKey('users.username') )
    following=db.Column(db.String, db.ForeignKey('users.username') )

class Likes(db.Model):
    __tablename__='like'
    l_id = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False)
    post_id = db.Column(db.Integer, db.ForeignKey('post.post_id') )
    username = db.Column(db.String, db.ForeignKey('users.username') )

