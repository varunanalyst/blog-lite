from flask import current_app as app
from datetime import datetime
from flask import Flask, redirect, render_template, request, make_response, send_file

from application.models import *
from application.functions import *

@app.route('/')
def home():
    p=None
    f=None
    user=getUser()
    if user:
        f=Follow.query.filter_by(follower=user.username)
        p=Posts.query.filter((Posts.username==Follow.following) & (Follow.follower==user.username) ).all()
        
    return render_template("index.html", user=user, p=p, f=f)

@app.route('/favicon.ico')
def favicon():
    return send_file('static/ico/favicon.ico')

@app.route('/register/', methods=['GET', 'POST'])
def register():
    user=getUser()
    if request.method=='GET':
        return render_template('register.html')
    else: # request.method=='POST'
        name=request.form.get('name')
        username=request.form.get('username')
        password=request.form.get('password')
        cpassword=request.form.get('cpassword')
        if password!=cpassword:
            return render_template('register.html', missmatch=True)
        if bool(Users.query.filter_by(username=username).first()):
            return render_template('register.html', duplicate=True)
        resp = make_response(redirect('/'))
        resp.set_cookie('username', username)
        resp.set_cookie('password', password)
        u=Users(username=username, name=name, password=password, followers=0, following=0, posts=0)
        db.session.add(u)
        db.session.commit()
        return resp

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method=='GET':
        return render_template('login.html')
    else: # if request.method=='POST'
        username=request.form['username']
        password=request.form['password']
        if not bool(Users.query.filter_by(username=username).first()):
            return render_template('login.html', not_found=True)
        u=Users.query.get(username)
        if u.password!=password:
            return render_template('login.html', wrong_password=True)
        name=u.name
        resp = make_response(redirect('/'))
        resp.set_cookie('username', username)
        resp.set_cookie('password', password)
        return resp

@app.route('/invalid')
def invalid():
    user=getUser()
    resp = make_response(render_template('invalid.html', user=user) )
    if not user:
        resp.set_cookie('username', max_age=0)
        resp.set_cookie('password', max_age=0)
    return resp

@app.route('/settings/', methods=['GET', 'POST'])
def settings():
    user=getUser()
    if not user:
        return redirect('/invalid')
    if request.method=='GET':
        return render_template("settings.html",user=user)
    else: # request.method=="post"
        if user.password != request.form["password"]:
            return render_template("settings.html",user=user, wrong_password=True)
        
        if request.form["Submit"]=="DELETE ACCOUNT":
            delete_user(user)
            return redirect('/logout')
        resp = make_response(redirect('/'))
        if request.form["Submit"]=="Change Password":
            p1=request.form["npassword"]
            p2=request.form["cpassword"]
            if p1!=p2:
                return render_template("settings.html",user=user, missmatch=True)
            user.password=p1
            db.session.commit()
            resp = make_response(redirect('/'))
            resp.set_cookie('password', p1)
            user.password=p1
            db.session.commit() 
        
        name=request.form["name"]
        user.name=name
        db.session.commit() 
        return resp

@app.route("/create", methods=['GET','POST'])
def create():
    user = getUser()
    if not user:
        return redirect("/invalid")
    if request.method=="GET":
        return render_template('create.html', user=user)
    else: # request.method=='POST'
        title=request.form['title']
        caption=request.form['caption']
        #The image address should have the post id as the prefix
        f=request.files['file']
        p=Posts(username=user.username, title=title, caption=caption, timestamp=str(datetime.now()), likes=0, comments=0)
        db.session.add(p)
        db.session.commit()
        image=str(p.post_id)+"_"+f.filename
        f.save("static/images/"+image)
        p.image=image
        user.posts+=1
        db.session.commit()
        return redirect('/post/'+str(p.post_id))

@app.route("/post/")
def all_posts():
    p=Posts.query.all()
    p.reverse()
    return render_template("posts.html",p=p, user=getUser())

@app.route("/post/<post_id>/", methods=['GET', 'POST'])
def view_post(post_id):
    if not bool(Posts.query.filter_by(post_id=post_id).first()):
        return "Does not exist!"
    p = Posts.query.get(post_id)
    user=getUser()
    if not user:
        liking = False
    else:
        liking=Likes.query.filter_by(post_id=post_id, username=user.username).first()
    author=Users.query.get(p.username)
    c = Comments.query.filter_by(post_id=post_id).all()
    if request.method=='POST':
        if not user:
            return redirect('/invalid')
        if request.form.get("like") == "Like":
            like_post(user, p)
            liking=True
        elif request.form.get("unlike") == "Unlike":
            unlike_post(user, p)
            liking=False
        elif request.form.get("submit") == "Add Comment":
            #return request.form
            add_comment(user, p, text=request.form.get('comment') )
        elif request.form.get("submit") == "Delete Comment":
            delete_comment(request.form.get("comment_id") )
    c = Comments.query.filter_by(post_id=post_id).all()
    return render_template('post.html', p=p, user=user, c=c, author=author, liking=bool(liking))



@app.route('/user/<username>', methods=['GET', 'POST'])
def user_profile(username):
    v_user=Users.query.filter_by(username=username).first()
    if not bool(v_user):
        return "Username "+str(username)+" does not exist!"
    following=False
    user=getUser()
    if user:
        following=Follow.query.filter_by(follower=user.username, following=username).first()
    if request.method=="POST":
        if not user:
            return redirect('/invalid')
        if request.form.get("follow") == "Follow":
            follow_user(follower=user, following=v_user)
            following=True
        elif request.form.get("unfollow")=="Unfollow":
            unfollow_user(follower=user, following=v_user)
            following=False
    p=Posts.query.filter_by(username=username).all()
    p.reverse()
    return render_template("profile.html", p=p, u=v_user, following=bool(following),user=user)

@app.route('/user/', methods=['GET', 'POST'])
def all_users():
    user=getUser()
    if request.method=="POST":
        user2 = Users.query.filter_by(username = request.form.get('username') ).first()
        if not user:
            return redirect('/invalid')
        if user2:
            if request.form.get('submit') == 'follow':
                follow_user(user, user2)
            elif request.form.get('submit') =='unfollow':
                unfollow_user(user, user2)

    following=[]
    if user:
        f=Follow.query.filter_by(follower=user.username).all()
        for i in f:
            following.append(i.following)
    q=request.args.get('q')
    if q:
        u=Users.query.filter(Users.username.like("%"+q+"%")).all()
    else:
        q=''
        u=Users.query.all()
    fl = following_list(user)
    return render_template("users.html", u=u,user=user,following=fl, q=q)

@app.route('/user/<username>/followers', methods=['GET', 'POST'] )
def user_followers(username):
    user=getUser()
    if request.method=="POST":
        user2 = Users.query.filter_by(username = request.form.get('username') ).first()
        if not user:
            return redirect('/invalid')
        if user2:
            if request.form.get('submit') == 'follow':
                follow_user(user, user2)
            elif request.form.get('submit') =='unfollow':
                unfollow_user(user, user2)
    following=[]
    if user:
        f=Follow.query.filter_by(follower=user.username).all()
        for i in f:
            following.append(i.following)
    u=Users.query.filter((Users.username==Follow.follower) & (Follow.following==username)).all()
    fl = following_list(user)
    return render_template("users.html", u=u,user=user,following=fl, viewing_followers_of=username)

@app.route('/user/<username>/following', methods=['GET', 'POST'] )
def user_following(username):
    user=getUser()
    if request.method=="POST":
        user2 = Users.query.filter_by(username = request.form.get('username') ).first()
        if not user:
            return redirect('/invalid')
        if user2:
            if request.form.get('submit') == 'follow':
                follow_user(user, user2)
            elif request.form.get('submit') =='unfollow':
                unfollow_user(user, user2)
    following=[]
    if user:
        f=Follow.query.filter_by(follower=user.username).all()
        for i in f:
            following.append(i.following)
    fl = following_list(user)
    u=Users.query.filter((Users.username==Follow.following) & (Follow.follower==username)).all()
    return render_template("users.html", u=u,user=user,following=fl, viewing_following_of=username)

@app.route("/post/<post_id>/edit", methods=['GET', 'POST'])
def edit(post_id):
    if not bool(Posts.query.filter_by(post_id=post_id).first()):
        return "Does not exist!"
    p=Posts.query.get(post_id)
    user=getUser()
    if user.username!=p.username:
        return "Access denied!"
    if request.method=="GET":
        return render_template("edit.html", p=p)
    else: # request.method=="POST'
        p.title=request.form['title']
        p.caption=request.form['caption']
        f=request.files['file']
        if bool(f.filename):
            os.remove("static/images/"+p.image)
            image=str(post_id)+"_"+f.filename
            f.save("static/images/"+image)
            p.image=image
        db.session.commit()
        return redirect('/post/'+str(post_id))

@app.route("/post/<post_id>/delete", methods=['GET', 'POST'])
def delete(post_id):
    if not bool(Posts.query.filter_by(post_id=post_id).first()):
        return "Does not exist!"
    p=Posts.query.get(post_id)
    user=getUser()
    if user.username!=p.username:
        return "Access denied!"
    if request.method=="GET":
        return render_template("delete.html", p=p, user=user)
    else: # request.method=="POST"
        if request.form.get("submit")=="YES":
            delete_post(p)
            return redirect("/")
        else:
            return redirect("/post/"+str(post_id))

@app.route('/logout/')
def logout():
    resp = make_response(redirect('/'))
    resp.set_cookie('username', max_age=0)
    resp.set_cookie('password', max_age=0)
    return resp
