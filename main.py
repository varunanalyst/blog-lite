import os
from datetime import datetime
from flask import Flask, redirect, render_template, request, make_response
from flask_sqlalchemy import SQLAlchemy
app = Flask(__name__)
current_dir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] =  "sqlite:///" +os.path.join(current_dir, "database.sqlite3")


from application.models import *


db.init_app(app)
app.app_context().push()

from application.controllers import *

if __name__ == '__main__':
	app.run(
		host='0.0.0.0',
		debug=True,
		port=8080
	) 